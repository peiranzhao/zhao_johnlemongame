﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    public WaypointPatrol[] allPatrols;

    public GameObject projectilePrefab;

    public Transform shotSpawn;

    public float shotSpeed = 10f;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();

        allPatrols = FindObjectsOfType<WaypointPatrol>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) {
            FreezeGhosts();
                }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            GameObject projectile = Instantiate(projectilePrefab, shotSpawn.transform.position, projectilePrefab.transform.rotation);

            Rigidbody projectileRB = projectile.GetComponent<Rigidbody>();
            projectileRB.velocity = transform.forward * shotSpeed;
        }
    }


    void FreezeGhosts()
    {
        for (int i=0; i < allPatrols.Length; i++)
        {
            if (allPatrols[i]!= null)
            {
                allPatrols[i].navMeshAgent.isStopped = true;
            }
        }
        CancelInvoke("EndFreeze");
        Invoke("EndFreeze", 2f);
    }

    void EndFreeze()
    {
        foreach (WaypointPatrol patrol in allPatrols)
        {
            if (patrol != null)
            {
                patrol.navMeshAgent.isStopped = false;
            }
        }
    }
        void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }
    void OnTriggerEnter(Collider other)
    {
        // ..and if the GameObject you intersect has the tag 'Pick Up' assigned to it..
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);

            for (int i = 0; i < allPatrols.Length; i++)
            {
                if (allPatrols[i] != null)
                {
                    allPatrols[i].navMeshAgent.isStopped = true;
                }
            }
            CancelInvoke("EndFreeze");
            Invoke("EndFreeze", 2f);

           
        }
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }
}